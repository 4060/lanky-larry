/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

/********* Version *********/
/* TalonSRX: 3.9
 * VictorSPX: 3.9
 * Phoenix Framework: 5.6.0 
 */

package org.usfirst.frc.team4060.robot;

import com.ctre.phoenix.motorcontrol.NeutralMode;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.DemandType;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;

import edu.wpi.first.wpilibj.CameraServer;
import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.PowerDistributionPanel;
import edu.wpi.first.wpilibj.Talon;
import edu.wpi.first.wpilibj.buttons.JoystickButton;


public class Robot extends IterativeRobot {
	/** Hardware */
	//As far as I know, It's all PWM, not can
	Joystick _gamepad = new Joystick(0);
	
	
	Talon testMotor = new Talon(0);
	
	
	

	@Override
	public void robotInit() {
		/* Don't use this for now */

			CameraServer.getInstance().startAutomaticCapture();
	
	}
	
	@Override
	public void teleopInit(){
		/* Disable motor controllers */
	//	_rightTopMaster.set(ControlMode.PercentOutput, 0);
		testMotor.set(0);
		
		
		
			
			
		
		/* Set Neutral mode */
	//	_leftTopMaster.setNeutralMode(NeutralMode.Brake);
	
		
		/* Configure output direction */
		//_leftTopMaster.setInverted(false);
		//_rightTopMaster.setInverted(true);
		testMotor.setInverted(true);
		
			
		
			
			
				JoystickButton mouthOpen = new JoystickButton(_gamepad, 1);
				
		
			
			
			
			
		
		
		
		System.out.println("This is a basic arcade drive using Arbitrary Feed Forward.");
	}
	
	@Override
	public void teleopPeriodic() {		
		/* Gamepad processing */
		double forward = -1 * _gamepad.getY();
		double rotate = 1 * _gamepad.getZ();
		double gameX = _gamepad.getX();
		double turn = _gamepad.getTwist();	
		double slider = _gamepad.getRawAxis(3);
		boolean mouthToggle = _gamepad.getTrigger();
			boolean motorsOn = true;
			
		forward = Deadband(forward);
		rotate = Deadband(rotate);
		turn = Deadband(turn);
		
		System.out.println(rotate);
		System.out.println(forward);

		
		// This is the place that all the buttons go, including the trigger
		
				
						if (motorsOn == false) {
						//	_leftTopMaster.set(ControlMode.PercentOutput, 0);
							
							
						}
				
				
				
				
		
		
		
		
		
		/* Basic Arcade Drive using PercentOutput along with Arbitrary FeedForward supplied by turn */
	/*
		
		if (rotate > 0 ) {
			rotate = rotate - 0.25;
		}
		
			if (rotate < 0 ) {
				rotate = rotate + 0.25;
			}
			
				if (forward > 0 ) {
					forward = forward - 0.25;
				}
		
					if (forward < 0 ) {
						forward = forward + 0.25;
					}
			
		*/		
					
					
					
										
											if (rotate > 0.25 || rotate < -0.25) {
												
												forward = 0;
											}
											
											if (forward > 0.50 || forward < -0.75) {
												
												rotate = 0;
											}
					
					
					
					
					
					
		if (rotate >  -0.25 && rotate < 0.25) { 
					//_leftTopMaster.set(ControlMode.PercentOutput, forward);
			testMotor.set(forward * 2);
				
		}
		
		else if (forward > -0.25 && forward < 0.025) {
			//_leftTopMaster.set(ControlMode.PercentOutput, rotate + 0.25);
			
			
		}
		
		
		
		
		else {
			//leftTopMaster.set(ControlMode.PercentOutput, 0);
			testMotor.set(0);
			
		}
	
		}
	
		
		
		
		//System.out.print(_leftTopMaster.getOutputCurrent());
		
	
		
		/*
		_leftTopMaster.set(ControlMode.Velocity, rotate+forward, DemandType.ArbitraryFeedForward, +turn);
		_rightTopMaster.set(ControlMode.Velocity, rotate+forward, DemandType.ArbitraryFeedForward, -turn);
		_leftBottomMaster.set(ControlMode.Velocity, rotate+forward, DemandType.ArbitraryFeedForward, +turn);
		_rightBottomMaster.set(ControlMode.Velocity, rotate+forward, DemandType.ArbitraryFeedForward, -turn);
		
		_leftTopSlave.set(ControlMode.Velocity, rotate+forward, DemandType.ArbitraryFeedForward, +turn);
		_rightTopSlave.set(ControlMode.Velocity, rotate+forward, DemandType.ArbitraryFeedForward, -turn);
		_leftBottomSlave.set(ControlMode.Velocity, rotate+forward, DemandType.ArbitraryFeedForward, +turn);
		_rightBottomSlave.set(ControlMode.Velocity, rotate+forward, DemandType.ArbitraryFeedForward, -turn);
		
		
		//if (mouthOpen == 1) {}
			
		
		*/
		
	

	/** Deadband 5 percent, used on the gamepad */
	double Deadband(double value) {
		/* Upper deadband */
		if (value >= +0.75) 
			return value;
		
		/* Lower deadband */
		if (value <= 0.75)
			return value;
		
		/* Outside deadband */
		return 0;
	}
	
	double RDeadband(double value) {
		/* Upper deadband */
		if (value >= +1) 
			return value;
		
		/* Lower deadband */
		if (value <= 1)
			return value;
		
		/* Outside deadband */
		return 0;
	}
}

